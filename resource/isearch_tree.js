var search = document.getElementById("search");
var search_result = document.getElementById("fuzzysearch2re_result");

function isearch_clear(){
    search.value = "";
    search_result.innerHTML = "";
}

function search_in_word(word, keywords){
    var start = 0;
    var w = word;
    for(var i = 0; i < keywords.length; i++){
        var k = keywords[i];
        //oconsole.log("search_in_word", word, k, keywords);

        var pos = word.indexOf(k);
        if(pos < 0){
            return keywords.slice(i, keywords.length);
        }
        w = w.substring(pos, pos+word.length);
    }
    return [];
}

function search_in_tree(name, tree, keywords, result, level){
    //console.log("search_in_tree_enter", level, tree, keywords, result);
    if(keywords.length == 0){
        //console.log("search_in_tree_length 0", tree, keywords, result);
        return result;
    }
    var remain = search_in_word(name, keywords);
    if(remain.length == 0){
        //console.log("search_in_tree_found", tree, keywords, result);
        result.push(tree);
        return result;
    }
    if(tree["type"] == "file"){
        return result;
    }
    //dir
    var klst = Object.keys(tree["children"]);
    for(var i = 0; i < klst.length; i++){
        var name = klst[i];
        search_in_tree(name, tree["children"][name], remain, result, level+1);
    }
    return result;
}

function isearch_tree(keywords){
    var result = search_in_tree(".", path_tree["children"]["."], keywords, [], 0);
    //console.log("result ", result);
    for(var i = 0; i < result.length; i++){
        print_tree(result[i]);
    }
}

function update_fuzzysearch2tree(){
    search_result.innerHTML = "";
    if(search.value == ""){
        return;
    }
    var keywords = search.value.split(" ");
    var current = window.location.href;
    var lst = current.split("/");
    var prefix = lst.slice(4, lst.length-1).join("/");

    isearch_tree(keywords);
    //var result = search_in_tree(".", path_tree["children"]["."], keywords, [], 0);
    //console.log("result ", result);

    var first = search_result.firstChild;
    first.className = first.className + " active";
}

search.onkeydown = function(event){
    if(typeof editor !== 'undefined'){
        if(editor.isFocused()){
            return;
        }
    }
    if(event.keyCode == 40){
        //down
        //TODO: move select next item
        //console.log($("#fuzzysearch2re_result .active"));
        event.preventDefault();
        var current = $("#fuzzysearch2re_result .active");
        if(current.is(':last-child')){
            //console.log("first");
            return;
        }
        current.removeClass("active").next().addClass("active");
        return;
    }
    else if(event.keyCode == 38){
        //up
        event.preventDefault();
        var current = $("#fuzzysearch2re_result .active");
        if(current.is(':first-child')){
            console.log("last");
            return;
        }
        current.removeClass("active").prev().addClass("active");
        return;
    }
    else if(event.keyCode == 27){
        //console.log("search keypress " + "escape");
        document.getElementById("search").blur();
        return;
    }
    else if(event.which == 13 || (event.ctrlKey && event.code == "KeyM")){
        //enter, ctrl-m
        var url = $("#fuzzysearch2re_result .active a").attr("href");
        console.log("follow " + url);
        window.location.href = url;
        return;
    }
    update_fuzzysearch2tree();
}

search.addEventListener("focus", function(event){
    update_fuzzysearch2tree();
});
search.addEventListener("blur", function(event){
    isearch_clear();
});

function go_parent_dir(){
    var current = window.location.href;
    var lst = current.split("/");
    var next;
    if(lst.length <= 4){
        //subdir (root)
        return;
    }
    if(lst[lst.length-1] == ""){
        //console.log("case1 " + lst.slice(2, lst.length-3));
        next = lst[0] + "//" + lst.slice(2,  lst.length-3).join("/");
    }
    else {
        //console.log("case2 " + lst.slice(2, lst.length-2));
        next = lst[0] + "//" + lst.slice(2, lst.length-2).join("/");
    }
    //console.log(next);
    //console.log(lst);
    return next;
}

document.getElementById("body").addEventListener("keypress", function(event){
    //console.log("keypress " + event.code + " " + event.which);
    if(typeof editor !== 'undefined'){
        if(editor.isFocused()){
            return;
        }
    }
    if(event.code == "Slash"){
        search.focus();
        event.preventDefault();
    }
    if(event.which == 94){
        // ^
        go_parent_dir();
        //window.location.href = go_parent_dir();
        return;
    }
});

function print_item(path, url){
    var li = document.createElement("li");
    var a = document.createElement("a");
    li.className = "list-group-item";
    a.href = url;
    a.innerText = path;
    li.appendChild(a);
    search_result.appendChild(li);
}

function print_tree(tree){
    print_item(tree["path"], tree["url"]);
    if(tree["type"] == "file"){
        return;
    }
    klst = Object.keys(tree["children"]);
    for(i = 0; i < klst.length; i++){
        var name = klst[i];
        print_tree(tree["children"][name]);
    }
}

// window.onload = function(){
//     var result = search_in_tree(".", path_tree["children"]["."], ["Doc"], [], 0);
//     //console.log("result ", result);
//     isearch_clear();
//     for(var i = 0; i < result.length; i++){
//         print_tree(result[i]);
//     }
// }
