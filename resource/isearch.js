function fuzzysearch2re_regexp (needle) {
    var nn = needle.replace(/ +/g, ".*");
    //TODO: escape special chars
    console.log("search reg " + nn);

    return new RegExp(nn);
}

function fuzzysearch2re (reg, haystack) {
    return haystack.match(reg)
}
var search = document.getElementById("search");
var search_result = document.getElementById("fuzzysearch2re_result");
var limit = 100;

function isearch_clear(){
    search.value = "";
    search_result.innerHTML = "";
}

function update_fuzzysearch2re_result(){
    search_result.innerHTML = "";
    var keyword = search.value;
    var current = window.location.href;
    var lst = current.split("/");
    var prefix = lst.slice(4, lst.length-1).join("/");

    var reg = fuzzysearch2re_regexp(keyword);
    var matched = 0;
    for(var i = 0; i < path_list.length; i++){
        var path = path_list[i]["path"];
        //console.log("search %s %s", path, prefix);
        if(0 < path.length && !path.startsWith(prefix)){
            continue;
        }
        if(!fuzzysearch2re(reg, path)){
            continue;
        }
        matched++;
        var li = document.createElement("li");
        var a = document.createElement("a");
        li.className = "list-group-item";
        a.href = path_list[i]["url"];
        a.innerText = path_list[i]["path"];
        li.appendChild(a);
        search_result.appendChild(li);
        if(limit <= matched){
            break;
        }
    }
    var first = document.getElementById("fuzzysearch2re_result").firstChild;
    first.className = first.className + " active";
}

document.getElementById("search").onkeyup = function(event){
    //console.log("search keypress " + event.which);
    if(event.keyCode == 27){
        console.log("search keypress " + "escape");
        document.getElementById("search").blur();
        return;
    }
    else if(event.keyCode == 40){
        //down
        //TODO: move select next item
        //console.log($("#fuzzysearch2re_result .active"));
        event.preventDefault();
        var current = $("#fuzzysearch2re_result .active");
        if(current.is(':last-child')){
            console.log("first");
            return;
        }
        current.removeClass("active").next().addClass("active");
        return;
    }
    else if(event.keyCode == 38){
        //up
        event.preventDefault();
        var current = $("#fuzzysearch2re_result .active");
        if(current.is(':first-child')){
            console.log("last");
            return;
        }
        current.removeClass("active").prev().addClass("active");
        return;
    }
    else if(event.which == 13 || (event.ctrlKey && event.code == "KeyM")){
        //enter, ctrl-m
        var url = $("#fuzzysearch2re_result .active a").attr("href");
        console.log("follow " + url);
        window.location.href = url;
        return;
    }
    update_fuzzysearch2re_result();
};
search.addEventListener("focus", function(event){
    if(0 < search.value.length){
        update_fuzzysearch2re_result();
    }
});
search.addEventListener("blur", function(event){
    isearch_clear();
});
function go_parent_dir(){
    var current = window.location.href;
    var lst = current.split("/");
    var next;
    if(lst.length <= 4){
        //subdir (root)
        return;
    }
    if(lst[lst.length-1] == ""){
        //console.log("case1 " + lst.slice(2, lst.length-3));
        next = lst[0] + "//" + lst.slice(2,  lst.length-3).join("/");
    }
    else {
        //console.log("case2 " + lst.slice(2, lst.length-2));
        next = lst[0] + "//" + lst.slice(2, lst.length-2).join("/");
    }
    console.log(next);
    console.log(lst);
    return next;
}

document.getElementById("body").addEventListener("keypress", function(event){
    console.log("keypress " + event.code + " " + event.which);
    if(event.code == "Slash"){
        document.getElementById("search").focus();
        event.preventDefault();
    }
    if(event.which == 94){
        // ^
        go_parent_dir();
        //window.location.href = go_parent_dir();
        return;
    }
});
function fuzzysearch2re_regexp (needle) {
    var nn = needle.replace(/ +/g, ".*");
    //TODO: escape special chars
    console.log("search reg " + nn);
    return new RegExp(nn);
}

function fuzzysearch2re (reg, haystack) {
    return haystack.match(reg)
}
var search = document.getElementById("search");
var search_result = document.getElementById("fuzzysearch2re_result");

function isearch_clear(){
    search.value = "";
    search_result.innerHTML = "";
}

function update_fuzzysearch2re_result(){
    search_result.innerHTML = "";
    var keyword = search.value;
    var current = window.location.href;
    var lst = current.split("/");
    var prefix = lst.slice(4, lst.length-1).join("/");

    var reg = fuzzysearch2re_regexp(keyword);
    for(var i = 0; i < path_list.length; i++){
        var path = path_list[i]["path"];
        //console.log("search %s %s", path, prefix);
        if(0 < path.length && !path.startsWith(prefix)){
            continue;
        }
        if(!fuzzysearch2re(reg, path)){
            continue;
        }
        var li = document.createElement("li");
        var a = document.createElement("a");
        li.className = "list-group-item";
        a.href = path_list[i]["url"];
        a.innerText = path_list[i]["path"];
        li.appendChild(a);
        search_result.appendChild(li);
    }

    var first = document.getElementById("fuzzysearch2re_result").firstChild;
    first.className = first.className + " active";

}

document.getElementById("search").onkeydown = function(event){
    if(event.keyCode == 40){
        //down
        //TODO: move select next item
        //console.log($("#fuzzysearch2re_result .active"));
        event.preventDefault();
        var current = $("#fuzzysearch2re_result .active");
        if(current.is(':last-child')){
            console.log("first");
            return;
        }
        current.removeClass("active").next().addClass("active");
        return;
    }
    else if(event.keyCode == 38){
        //up
        event.preventDefault();
        var current = $("#fuzzysearch2re_result .active");
        if(current.is(':first-child')){
            console.log("last");
            return;
        }
        current.removeClass("active").prev().addClass("active");
        return;
    }
}

document.getElementById("search").onkeyup = function(event){
    //console.log("search keypress " + event.which);
    if(event.keyCode == 27){
        console.log("search keypress " + "escape");
        document.getElementById("search").blur();
        return;
    }
    if(event.keyCode == 40 || event.keyCode == 38){
        //up down
        return;
    }
    //     //down
    //     //TODO: move select next item
    //     //console.log($("#fuzzysearch2re_result .active"));
    //     event.preventDefault();
    //     var current = $("#fuzzysearch2re_result .active");
    //     if(current.is(':last-child')){
    //         console.log("first");
    //         return;
    //     }
    //     current.removeClass("active").next().addClass("active");
    //     return;
    // }
    // else if(event.keyCode == 38){
    //     //up
    //     event.preventDefault();
    //     var current = $("#fuzzysearch2re_result .active");
    //     if(current.is(':first-child')){
    //         console.log("last");
    //         return;
    //     }
    //     current.removeClass("active").prev().addClass("active");
    //     return;
    // }
    else if(event.which == 13 || (event.ctrlKey && event.code == "KeyM")){
        //enter, ctrl-m
        var url = $("#fuzzysearch2re_result .active a").attr("href");
        console.log("follow " + url);
        window.location.href = url;
        return;
    }
    update_fuzzysearch2re_result();
};
document.getElementById("search").addEventListener("focus", function(event){
    update_fuzzysearch2re_result();
});
document.getElementById("search").addEventListener("blur", function(event){
    isearch_clear();
});
function go_parent_dir(){
    var current = window.location.href;
    var lst = current.split("/");
    var next;
    if(lst.length <= 4){
        //subdir (root)
        return;
    }
    if(lst[lst.length-1] == ""){
        //console.log("case1 " + lst.slice(2, lst.length-3));
        next = lst[0] + "//" + lst.slice(2,  lst.length-3).join("/");
    }
    else {
        //console.log("case2 " + lst.slice(2, lst.length-2));
        next = lst[0] + "//" + lst.slice(2, lst.length-2).join("/") + "/";
    }
    console.log(next);
    return next;
}

document.getElementById("body").addEventListener("keypress", function(event){
    console.log("keypress " + event.code + " " + event.which);
    if(event.code == "Slash"){
        document.getElementById("search").focus();
        event.preventDefault();
    }
    if(event.which == 94){
        // ^
        window.location.href = go_parent_dir();
        //window.location.href = go_parent_dir();
        return;
    }
});
