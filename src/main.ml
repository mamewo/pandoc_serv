open Printf
open Getopt
open Jingoo

let subdir = "pandoc"
let default_lang = "en"
let root_path = sprintf "/%s/" subdir
let resource_path_regexp = Str.regexp (sprintf "^%s\\(.*\\)$" root_path)
let root_doc_path = ref "."
let dir_template = [%blob "./list.tmpl"]
let edit_template = [%blob "./edit.tmpl"]

let css_opt = ref ""
let header_opt = ref ""
let lang_opt = ref ""
let additional_opt = ref ""
let redmine_url_opt = ref ""
let index_file = "index.txt"

(* per session , cookie? *)

let basedir = ref []
let concat_mode_opt = ref false

let ext2mimetype = [
    (".css", "text/css");
    (".js", "text/javascript");
    (".png", "image/png");
    (".jpg", "image/jpeg");
    (".jpeg", "image/jpeg");
    (".gif", "image/gif");
    (".html", "text/html; charset=UTF-8");
    (".txt", "text/plain");
    (".pdf", "application/pdf");
    (".doc", "application/msword");
    (".docx", "application/msword");
  ]

let add_channel buf chan =
  try
    while true do
      let l = input_line chan in
      Buffer.add_string buf (l ^ "\n")
    done;
  with End_of_file ->
    close_in chan
;;

let add_file buf filename =
  let chan = open_in filename in
  add_channel buf chan
;;

let pandoc_command file_opt =
  let header =
    if !header_opt <> "" then
      "-H " ^ (Core.Filename.realpath !header_opt)
    else
      "" in
  let css =
    if !css_opt <> "" then
      "-c " ^ (Core.Filename.realpath !css_opt)
    else
      "" in
  let filename =
    match file_opt with
      Some f -> Core.Filename.basename f
    | None -> "" in
  (* TODO; add self-contained option *)
  let cmd = Printf.sprintf "pandoc %s --toc --number-sections %s %s %s" header css !additional_opt filename in
  Netlog.logf `Debug "pandoc command: %s" cmd;
  cmd
;;

let pandoc_command_msoffice typ file_opt =
  let css =
    if !css_opt <> "" then
      "-c " ^ !css_opt
    else
      "" in
  let filename =
    match file_opt with
      Some f -> f
    | None -> "" in
  (* TODO; add self-contained option *)
  let cmd = Printf.sprintf "pandoc --self-contained -t html --toc --number-sections %s %s %s" css !additional_opt filename in
  Netlog.logf `Debug "pandoc dir: %s" (Sys.getcwd ());
  Netlog.logf `Debug "pandoc command: %s" cmd;
  cmd
;;

let convert_content cmd localpath =
  let buf = Buffer.create 1024 in
  let olddir = Sys.getcwd () in
  let dirname = Filename.dirname localpath in
  (* changedir to resolve relative resources (img) *)
  Sys.chdir dirname;
  let p = Unix.open_process_in cmd in
  add_channel buf p;
  ignore @@ Unix.close_process_in p;
  Sys.chdir olddir;
  buf
;;

let serv_pandoc (cgi : Netcgi.cgi_activation) localpath =
  try
    let cmd = pandoc_command (Some localpath) in
    Netlog.logf `Debug "serv command: %s" cmd;
    let buf = convert_content cmd localpath in
    cgi # environment # set_output_header_field "Content-Type" "text/html; charset=UTF-8";
    cgi # output # output_string (Buffer.contents buf);
    cgi # output # commit_work();
  with e ->
    Netlog.logf `Err "Uncaught exception: %s　%s" (Printexc.to_string e) (Printexc.get_backtrace ());
;;

let serv_pandoc_msoffice (cgi : Netcgi.cgi_activation) localpath =
  try
    Netlog.logf `Debug "msoffice: %s" localpath;
    let typ = String.sub localpath 1 @@ String.length localpath - 1 in
    let basename = Filename.basename localpath in
    let cmd = pandoc_command_msoffice typ (Some basename) in
    let buf = convert_content cmd localpath in
	cgi # environment # set_output_header_field "Content-Type" "text/html; charset=UTF-8";
    cgi # output # output_string (Buffer.contents buf);
    cgi # output # commit_work();
  with e ->
    Netlog.logf `Err "Uncaught exception: %s %s" (Printexc.to_string e) (Printexc.get_backtrace ());
;;

let serv_jupyter  (cgi : Netcgi.cgi_activation) localpath =
  try
    let cwd = Sys.getcwd () in
    let convert_command = Filename.concat cwd "convert_ipynb.sh" in
    let base = Filename.basename localpath in
    let cmd = Printf.sprintf "%s \"%s\"" convert_command base in
    let buf = convert_content cmd localpath in
	cgi # environment # set_output_header_field "Content-Type" "text/html; charset=UTF-8";
    cgi # output # output_string (Buffer.contents buf);
    cgi # output # commit_work();
  with e ->
    Netlog.logf `Err "Uncaught exception: %s　%s" (Printexc.to_string e) (Printexc.get_backtrace ())
;;

let last_char s =
  let l = String.length s in
  s.[l-1]
;;

let list_dir (cgi : Netcgi.cgi_activation) localpath =
  (* TODO: config *)
  Netlog.logf `Debug "local, root %s %s" localpath !root_doc_path;
  let ignore_list =
    let local =
      if last_char localpath != '/' then
        localpath ^ "/"
      else
        localpath in
    let root =
      if last_char !root_doc_path != '/' then
        !root_doc_path ^ "/"
      else
        !root_doc_path in
    if local = root then
      [".."; "."; ".DS_Store"]
    else
      ["."; ".DS_Store"] in
  let dir = Unix.opendir localpath in
  let rec iter l =
    try
      let entry = Unix.readdir dir in
      (* Netlog.logf `Debug "entry %s" entry; *)
      (* let tmp = Filename.concat localpath entry in *)
      if List.mem entry ignore_list then
        iter l
      else
        try
          let st = Unix.stat (Filename.concat localpath entry) in
          let (href, is_dir) =
            if st.Unix.st_kind = Unix.S_DIR then
              (entry ^ "/", true)
            else
              (entry, false) in
          let obj = Jg_types.Tobj [("name", Jg_types.Tstr entry);
                                   ("is_dir", Jg_types.Tbool is_dir);
                                   ("href", Jg_types.Tstr href)] in
          iter (obj::l)
        with _ ->
          iter l
    with End_of_file ->
      List.sort compare l in
  let model = [("list", Jg_types.Tlist (iter []));
               ("subdir", Jg_types.Tstr subdir)] in
  Unix.closedir dir;
  cgi # output # output_string (Jg_template.from_string dir_template ~models:model);
  cgi # output # commit_work()
;;

let find_resouce path =
  (* Netlog.logf `Debug "find_resource %s" path; *)
  let rec iter baselist =
    match baselist with
      [] -> raise Not_found
    | base::tl ->
       let localpath = Filename.concat base path in
       (* Netlog.logf `Debug "find_resource %s %s" base path; *)
       if Sys.file_exists localpath then
         localpath
       else
         iter tl in
  iter !basedir
;;

let concat_pandoc (cgi : Netcgi.cgi_activation) localpath =
  let pandoc_cmd = pandoc_command None in
  let cmd = Printf.sprintf "find %s -name \"*.md\" | sort | xargs -P1 awk 'FNR==1{print \"\"}1' | %s" localpath pandoc_cmd in
  Netlog.logf `Debug "command %s" cmd;
  let buf = Buffer.create 1024 in
  let p = Unix.open_process_in cmd in
  add_channel buf p;
  ignore @@ Unix.close_process_in p;
  cgi # output # output_string (Buffer.contents buf);
  cgi # output # commit_work();
;;

(* TODO: dirhandler, css, js, txt, handler (plain)*)
let content_handler_table = [
    (".md", serv_pandoc);
    (".md.html", serv_pandoc);
    (".docx", serv_pandoc_msoffice);
    (".pptx", serv_pandoc_msoffice);
    (".ipynb", serv_jupyter);
  ]
;;

let assoc_table_find f table =
  let rec iter = function
      [] -> None
    | hd::tl ->
       match f hd with
         Some _ as s -> s
       | None -> iter tl in
  iter table
;;

let on_request notification =
  (* This function is called when the full HTTP request has been received. For
   * simplicity, we create a [std_activation] to serve the request.
   *
   * An advanced implementation could set up further notifications to get informed
   * whenever there is space in the response buffer for additional output.
   * Currently, data is fully buffered (first
   * in the transactional buffer, then in the response buffer), and only when
   * the message is complete, the transmission to the client starts.
   * By generating only the next part of the response when there is space in
   * the response buffer, the advanced implementation can prevent that the
   * buffers become large.
   *)
  (try
     let env = notification # environment in
     let request_uri = env # cgi_request_uri |> Uri.of_string in
     let path = Uri.path request_uri in
     (* resourcce *)
     if not (Str.string_match resource_path_regexp path 0) then
	   let cgi =
         Netcgi_common.cgi_with_args
           (new Netcgi_common.cgi)
           (env :> Netcgi.cgi_environment)
           Netcgi.buffered_transactional_outtype
           env#input_channel
           (fun _ _ _ -> `Automatic) in
       cgi # output # commit_work();
     else
       let localpath = Filename.concat !root_doc_path (Str.matched_group 1 path) in
       Netlog.logf `Debug "path %s,  local_path %s" path localpath;
       let find_handler (suffix, handler) =
         if Filename.check_suffix path suffix then
           Some handler
         else
           None in
       match assoc_table_find find_handler content_handler_table with
         Some handler ->
	      let cgi =
            Netcgi_common.cgi_with_args
              (new Netcgi_common.cgi)
              (env :> Netcgi.cgi_environment)
              Netcgi.buffered_transactional_outtype
              env#input_channel
              (fun _ _ _ -> `Automatic) in
          handler cgi localpath
       | None ->
          (* TODO; handle not found*)
          let st = Unix.stat localpath in
          if st.Unix.st_kind = Unix.S_DIR then
	        let cgi =
              Netcgi_common.cgi_with_args
                (new Netcgi_common.cgi)
                (env :> Netcgi.cgi_environment)
                Netcgi.buffered_transactional_outtype
                env#input_channel
                (fun _ _ _ -> `Automatic) in
            env # set_output_header_field "Content-Type" "text/html; charset=UTF-8";
            (* basedir := []; *)
            if !concat_mode_opt then
              concat_pandoc cgi localpath
            else
	          list_dir cgi localpath;
          else
            let localpath = find_resouce localpath in
            let st = Unix.stat localpath in
      	    let length = st.Unix.st_size |> Int64.of_int in
            let mode =
              match Uri.get_query_param request_uri "mode" with
                Some m ->
                if m = "edit" then
                  "edit"
                else
                  "view"
              | None -> "view" in
            let ace_editor =
              match Uri.get_query_param request_uri "ace_editor" with
                Some m ->
                if List.mem m ["vim"; "emacs"; "sublime"; "ace"; "vscode"] then
                  m
                else
                  "emacs"
              | None -> "emacs" in
            (* Netlog.logf `Debug "mode %s %d %s" mode (List.length (env # cgi_properties)) (env # cgi_query_string); *)
            if mode = "edit" then
              let content =
                let buf = Buffer.create 1024 in
                let p = open_in localpath in
                add_channel buf p;
                Buffer.contents buf in
	          let cgi =
                Netcgi_common.cgi_with_args
                  (new Netcgi_common.cgi)
                  (env :> Netcgi.cgi_environment)
                  Netcgi.buffered_transactional_outtype
                  env#input_channel
                  (fun _ _ _ -> `Automatic) in
              let basename = Filename.basename localpath in
              let suffix_to_mode = [
                  (".md", "markdeep");
                  (".html", "html");
                  (".py", "python");
                  (".c", "c");
                  (".ml", "ocaml");
                  (".mli", "ocaml");
                  (".el", "lisp");
                  (".css", "css");
                  (".sh", "sh");
                  (".js", "javascript");
                  (".sql", "sql");
                ] in
              let rec get_mode = function
                  [] -> "text"
                | (suff, mode)::tl ->
                   if Filename.check_suffix basename suff then
                     mode
                   else
                     get_mode tl in
              let ace_mode =
                if basename = "Dockerfile" then
                  "dockerfile"
                else if basename = "Makefile" then
                  "makefile"
                else
                  get_mode suffix_to_mode in
              let model = [("content", Jg_types.Tstr content);
                           ("ace_editor", Jg_types.Tstr ace_editor);
                           ("ace_mode", Jg_types.Tstr ace_mode);
                           ("subdir", Jg_types.Tstr subdir)] in
              let resp_body = Jg_template.from_string ~models:model edit_template in
                begin
                  cgi # environment # set_output_header_field "Content-Type" "text/html; charset=UTF-8";

                  cgi # output # output_string resp_body;
                  cgi # output # commit_work();
                end
            else
              let fd = Unix.openfile localpath [Unix.O_RDONLY; Unix.O_NONBLOCK] 0o640 in
              let typ =
                try
                  let (ext, typ) =
                    List.find (fun (ext, typ) -> Filename.check_suffix localpath ext) ext2mimetype in
                  typ
                with Not_found ->
                  "text/plain; charset=UTF-8" in
              begin
                env # set_output_header_field "Content-Type" typ;
                (* selct content *)
      	        env # send_file fd length;
              end
  with e ->
    Netlog.logf `Err "Uncaught exception: %s　%s"
                (Printexc.to_string e)
                (Printexc.get_backtrace ()));
  notification # schedule_finish()
;;

let on_request_header (notification : Nethttpd_engine.http_request_header_notification) =
  (* After receiving the HTTP header: We always decide to accept the HTTP body, if any
   * is following. We do not set up special processing of this body, it is just
   * buffered until complete. Then [on_request] will be called.
   *
   * An advanced server could set up a further notification for the HTTP body. This
   * additional function would be called whenever new body data arrives. (Do so by
   * calling [notification # environment # input_ch_async # request_notification].)
   *)
  notification # schedule_accept_body ~on_request ();
;;

let serve_connection ues fd =
  (* Creates the http engine for the connection [fd]. When a HTTP header is received
   * the function [on_request_header] is called.
   *)
  let config = Nethttpd_engine.default_http_engine_config in
  Unix.set_nonblock fd;
  let _ =
    new Nethttpd_engine.http_engine ~on_request_header () config fd ues in
  ()
;;

let rec accept ues srv_sock_acc =
  (* This function accepts the next connection using the [acc_engine]. After the
   * connection has been accepted, it is served by [serve_connection], and the
   * next connection will be waited for (recursive call of [accept]). Because
   * [server_connection] returns immediately (it only sets the callbacks needed
   * for serving), the recursive call is also done immediately.
   *)
  let acc_engine = srv_sock_acc # accept() in
  Uq_engines.when_state ~is_done:(fun (fd,fd_spec) ->
			        if srv_sock_acc # multiple_connections then (
			          serve_connection ues fd;
			          accept ues srv_sock_acc
                                   ) else
				  srv_sock_acc # shut_down())
                        ~is_error:(fun _ -> srv_sock_acc # shut_down())
                        acc_engine;
;;

let start bind_address port =
  (* We set up [lstn_engine] whose only purpose is to create a server socket listening
   * on the specified port. When the socket is set up, [accept] is called.
   *)
  Netlog.logf `Debug "Listening on port %d" port;
  let ues = Unixqueue.create_unix_event_system () in
  (* Unixqueue.set_debug_mode true; *)
  let opts = { Uq_server.default_listen_options with
     Uq_engines.lstn_reuseaddr = true } in
  let lstn_engine =
    Uq_server.listener
      (`Socket(`Sock_inet(Unix.SOCK_STREAM, bind_address, port) ,opts)) ues in
  Uq_engines.when_state ~is_done:(accept ues) lstn_engine;
  (* Start the main event loop. *)
  Unixqueue.run ues
;;

let conf_debug() =
  (* Set the environment variable DEBUG to either:
       - a list of Netlog module names
       - the keyword "ALL" to output all messages
       - the keyword "LIST" to output a list of modules
     By setting DEBUG_WIN32 additional debugging for Win32 is enabled.
   *)
  let debug = try Sys.getenv "DEBUG" with Not_found -> "" in
  if debug = "ALL" then
    Netlog.Debug.enable_all()
  else if debug = "LIST" then (
    List.iter print_endline (Netlog.Debug.names());
    exit 0
  )
  else (
    let l = Netstring_str.split (Netstring_str.regexp "[ \t\r\n]+") debug in
    List.iter
      (fun m -> Netlog.Debug.enable_module m)
      l
  );
  if (try ignore(Sys.getenv "DEBUG_WIN32"); true with Not_found -> false) then
    Netsys_win32.Debug.debug_c_wrapper true
;;

let print_usage() =
  print_endline ("Usage: "^Sys.argv.(0));
  print_endline ("Description: web server which serves markdown file as pandoc");
  print_endline ("Options:");
  print_endline ("  -h: print help");
  print_endline ("  -p port: bind port");
  print_endline ("  -b host: bind address");
  print_endline ("  -c css_file_path: customize css");
  print_endline ("  -l lang: set language (default: ja)");
  print_endline ("  -O opt: options")
;;

let main () =
  let default_port = 8000 in
  let bind_address_opt = ref "" in
  let port_opt = ref "" in
  let args = ref [] in
  let specs = [
      ('b', "", None, (atmost_once bind_address_opt (Error "only one bind address")));
      ('p', "", None, (atmost_once port_opt (Error "only one port")));
      ('h', "", Some(fun () -> print_usage(); exit 0), None);
      ('H', "header", None, (atmost_once header_opt (Error "only one header")));
      ('c', "css", None, (atmost_once css_opt (Error "only one css")));
      ('C', "concat", (set concat_mode_opt true), None);
      ('l', "lang", None, (atmost_once lang_opt (Error "only one language")));
      ('O', "opt", None, (atmost_once additional_opt (Error "only one opt")));
      ('R', "redmine", None, (atmost_once redmine_url_opt (Error "only one redmine url opt")));
    ] in
  Getopt.parse_cmdline specs (fun x -> args := !args@[x]);
  let bind_address =
    if !bind_address_opt = "" then
      Unix.inet_addr_loopback
    else
      Unix.inet_addr_of_string !bind_address_opt in
  conf_debug();
  let port =
    if !port_opt = "" then
      default_port
    else
      int_of_string !port_opt in
  if !lang_opt = "" then
    begin
      lang_opt :=
        try
          let lang = Sys.getenv("LANG") in
          (match Str.split (Str.regexp "_") lang with
             hd::_ -> hd
           | _ -> default_lang)
        with Not_found ->
          default_lang
    end;
  (match !args with
     hd::_ -> root_doc_path := hd
   | [] -> ());
  basedir := [Core.Filename.realpath !root_doc_path];
  Netsys_signal.init();
  start bind_address port
;;

let () =
  Printexc.record_backtrace true;
  main ()
;;
