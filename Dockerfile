#FROM ocaml/opam:ubuntu
FROM ocaml/opam2:4.06

RUN sudo apt-get update && \
sudo apt-get install --no-install-recommends -qq -yy libpcre3-dev pkg-config m4 pandoc && \
sudo rm -rf /var/lib/apt/lists/*

RUN opam repo add opam_default https://opam.ocaml.org
RUN sudo mkdir -p /home/opam/work
# COPY . /home/opam/work
RUN sudo chown -R opam /home/opam/work

USER opam

RUN sudo apt update && sudo apt install -y python3-venv
RUN python3 -m venv /home/opam/venv && . /home/opam/venv/bin/activate && pip install jupyter

WORKDIR /home/opam/work
RUN opam switch 4.06
COPY setup-opam.sh /home/opam/work/
RUN sh setup-opam.sh

COPY OCamlMakefile Makefile src /home/opam/work/
RUN mkdir /home/opam/work/src
COPY src /home/opam/work/src

RUN eval `opam config env` && make clean all
VOLUME /home/opam/work/data
WORKDIR /home/opam/work/data

# search
#RUN apt-get update && apt-get install -y perl wget libfile-mmagic-perl namazu2 namazu2-index-tools

#ENTRYPOINT ["/home/opam/work/pdserv"]
