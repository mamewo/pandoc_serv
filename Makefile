OCAMLMAKEFILE=OCamlMakefile

TARGET=pdserv
#OPTS = RESULT=$(TARGET) SOURCES=src/main.ml THREADS=1 PACKS=nethttpd,uri,getopt,jingoo,ppx_blob,core,core_kernel ANNOTATE=yes LDFLAGS=-static
OPTS = RESULT=$(TARGET) SOURCES=src/main.ml THREADS=1 PACKS=nethttpd,uri,getopt,jingoo,ppx_blob,core,core_kernel ANNOTATE=yes

all: $(TARGET)

$(TARGET): src/main.ml
	make -f $(OCAMLMAKEFILE) $(OPTS) nc

clean:
	make -f $(OCAMLMAKEFILE) $(OPTS) clean
