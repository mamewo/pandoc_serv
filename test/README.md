% Pandoc server
% Takashi Masuyama <mamewotoko@gmail.com>
% May 13, 2018

Overview
========

```
Usage: ./pdserv [root_doc_path]
Description: web server which serves markdown file as markdeep
Options:
  -h: print help
  -p port: bind port (defalt: 8000)
  -b host: bind address (defalt: 127.0.0.1)
  -c css_file_path: customize css (default: none)
```

Build
=====
1.

  ```bash
  make
  ```

Run
===
1. 

  ```bash
  ./mdserv
  ```
  
2. browse [http://localhost:8000/pandoc/README.md](http://localhost:8000/pandoc/README.md)

Docker run
==========
1. Build docker container

  ```bash
  docker-compose build
  ```

2. Edit docker-compose.yml to set options, then run

  ```bash
  docker-compose up -d nav
  ```

browse http://localhost:8003/pandoc/README.md

3. Stop

  ```
  docker-compose down
  ```

Image!
=========
![anko @ nekorobi](img/DSC04771.jpg)

TODO
====
* concat markdown files to print
  * resolve path of inline image
  * resolve link of headings

Reference
=========
* [Pandoc](https://pandoc.org/)

----
Takashi Masuyama < mamewotoko@gmai.com >  

http://mamewo.ddo.jp/
