#! /usr/bin/env python3
import json
import os
import sys
# TODO write in OCaml

def path_to_url(path):
    # TODO: config
    BASE = "/pandoc/"
    if path.startswith("./"):
        path = path[2:]
    if not BASE.endswith("/"):
        BASE = BASE + "/"
    return BASE + path

def simple_index(target_dir):
    # TODO: visit on virtual filesystem which webserver servers
    result = []
    os.chdir(target_dir)
    for curdir, dirs, files in os.walk("."):
        for f in files + dirs:
            path = os.path.join(curdir, f)
            if path == "..":
                continue
            if path.startswith("./"):
                path = path[2:]
            if ".git/" in path:
                continue
            url = path_to_url(path)
            data = { "path": path,
                     "url": url }
            result.append(data)

    return result


def get_path(result, path):
    current = result
    if path == "./":
        path = "."

    for p in path.split("/"):
        # print("p", p)
        current = current["children"][p]
    return current


def tree_index(target_dir):
    result = {
        "type": "dir",
        "path": "root",
        "url": "root",
        "children": {
            ".": {
                "type": "dir",
                "path": ".",
                "url": ".",
                "children": {}
            }
        }
    }
    os.chdir(target_dir)
    for curdir, dirs, files in os.walk("."):
        # print(curdir)
        current = get_path(result, curdir)
        if curdir.startswith("./"):
            curdir = curdir[2:]
        for f in dirs:
            path = os.path.join(curdir, f)
            if f in [".", ".."]:
                continue
            url = path_to_url(path)
            # print("f",type(f))
            # print("path",type(path))
            # print("url",type(url))
            current["children"][f] = {
                "type": "dir",
                "name": f,
                # "path": path,
                # "url": url,
                "children": {}
            }

        for f in files:
            path = os.path.join(curdir, f)
            url = path_to_url(path)
            # print("f",type(f))
            # print("path",type(path))
            # print("url",type(url))

            current["children"][f] = {
                "type": "file",
                # "path": path,
                # "url": url
                "name": f
            }

    return result

if __name__ == "__main__":
    target_dir = os.path.abspath(sys.argv[1])
    result = tree_index(target_dir)
    # print(result)
    print("const path_list = ")
    print(json.dumps(result, indent=2))
    print(";")
