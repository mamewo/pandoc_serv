#! /bin/bash

SCRIPT_DIR=$(realpath $(dirname $0))
#. ${SCRIPT_DIR}/venv/bin/activate
# container
. /home/opam/venv/bin/activate
jupyter nbconvert  --stdout "$1"

# todo modify path
# jupyter nbconvert  --stdout "$1" | sed 's!</body>!<script src="anchor.js"></script><script>anchors.add("div.cell")</script></body>!'
